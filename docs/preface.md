# 前言

## 概述

对YT-FP人像大平台的产品定位、产品界面、功能信息、功能配置进行了介绍，以便读者全面了解该产品。



## 产品版本

与本文档相对应的产品版本如下所示。

| 产品名称        | 产品版本 |
| :-------------- | :------- |
| YT-FP人像大平台 | V2.0.1   |



<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;border-color:#aabcfe;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aabcfe;color:#669;background-color:#e8edff;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aabcfe;color:#039;background-color:#b9c9fe;}
.tg .tg-hmp3{background-color:#D2E4FC;text-align:left;vertical-align:top}
.tg .tg-baqh{text-align:center;vertical-align:top}
.tg .tg-mb3i{background-color:#D2E4FC;text-align:right;vertical-align:top}
.tg .tg-lqy6{text-align:right;vertical-align:top}
.tg .tg-0lax{text-align:left;vertical-align:top}
</style>
<table class="tg">
  <tr>
    <th class="tg-baqh" colspan="6">Results</th>
  </tr>
  <tr>
    <td class="tg-hmp3">No</td>
    <td class="tg-hmp3">Competition</td>
    <td class="tg-hmp3">John</td>
    <td class="tg-hmp3">Adam</td>
    <td class="tg-hmp3">Robert</td>
    <td class="tg-hmp3">Paul</td>
  </tr>
  <tr>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">Swimming</td>
    <td class="tg-lqy6" colspan="2">1:30</td>
    <td class="tg-lqy6">1:15</td>
    <td class="tg-lqy6">1:41</td>
  </tr>
  <tr>
    <td class="tg-hmp3">2</td>
    <td class="tg-hmp3">Running</td>
    <td class="tg-mb3i">15:30</td>
    <td class="tg-mb3i">14:10</td>
    <td class="tg-mb3i" colspan="2" rowspan="2">15:45</td>
  </tr>
  <tr>
    <td class="tg-0lax">3</td>
    <td class="tg-0lax">Shooting</td>
    <td class="tg-lqy6">70%</td>
    <td class="tg-lqy6">55%</td>
  </tr>
</table>



## 读者对象

本文档主要适用于以下工程师：

- 赋能
- 实施



## 修订记录

修改记录累积了每次文档更新的说明。最新版本的文档包含以前所有文档版本的更新内容。

- **文档版本 01 (2018-05-08)**

  第一次正式发布。本资料版本对应软件版本为V2.0.1。



# 产品介绍

## 产品简介

依图人像大平台以依图自主研发、世界领先的超高精度人脸识别算法为核心，基于公安用户业务流程，针对安防行业不同人种、复杂环境、海量数据等极具挑战性的问题，提供从宏观城市整体安全掌控到微观个人精准时空轨迹描绘的智能人像整体解决方案。